package de.mjkd.bpm;

/**
 * Klasse mit für den Prozess benötigte Statische Variablen.
 * Über die Namen werden der Vertrag und die ReferenzID im System bekannt gemacht
 *
 */
public class ProcessConstants {

    public static final String VARIABLE_NAME_VERTRAG = "vertrag";
    public static final String VARIABLE_NAME_REFID = "documentReferenceId";
}
