package de.mjkd.bpm.model;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Klasse die den Vertrag repräsentiert. Beinhaltet Versicherungsnehmer, Auto und eine eindeutige Nummer sowie das
 * "gewählte" Produkt
 */
public class Vertrag {

    private Car auto;
    private Person versicherungsnehmer;

    private String produkt;

    private String vertragsNummer = nextApplicationNummer();

    private double beitrag;

    public static int counter = 0;

    public static String nextApplicationNummer() {
        // Achtung: Nur für Demo-Zwecke für eine lesbare Nummer - in real: UUID!
        if (counter == 0) {
            counter = Calendar.getInstance().get(Calendar.MINUTE) + Calendar.getInstance().get(Calendar.SECOND);
        } else {
            counter++;
        }
        String result = "A-" + Calendar.getInstance().get(Calendar.DAY_OF_YEAR) + counter;
        return result;
    }

    public Car getAuto() {
        return auto;
    }

    public void setAuto(Car auto) {
        this.auto = auto;
    }

    public Person getVersicherungsnehmer() {
        return versicherungsnehmer;
    }

    public void setVersicherungsnehmer(Person versicherungsnehmer) {
        this.versicherungsnehmer = versicherungsnehmer;
    }

    public String getProdukt() {
        return produkt;
    }

    public void setProdukt(String produkt) {
        this.produkt = produkt;
    }

    public String getVertragsNummer() {
        return vertragsNummer;
    }

    public void setVertragsNummer(String vertragsNummer) {
        this.vertragsNummer = vertragsNummer;
    }

    public double getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(double beitrag) {
        this.beitrag = beitrag;
    }

    public String getBeitragString() {
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.GERMAN);
        return n.format(beitrag / 100.0);
    }

    public void setBeitragString(String s) {
        // ignore - currently needed because JsonIgnore configuration not yet done
    }

    @Override
    public String toString() {
        return "Vertrag{" +
                "auto=" + auto +
                ", versicherungsnehmer=" + versicherungsnehmer +
                ", produkt='" + produkt + '\'' +
                ", vertragsNummer='" + vertragsNummer + '\'' +
                ", beitrag=" + beitrag +
                '}';
    }
}
