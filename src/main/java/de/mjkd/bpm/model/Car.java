package de.mjkd.bpm.model;

/**
 * Representation eines "Autos" für den Vertrag.
 * Beinhaltet die Art des Autos und die Schadensfreiheitsklasse
 */
public class Car {

    private String carType;
    private int sfClass;

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public int getSfClass() {
        return sfClass;
    }

    public void setSfClass(int sfClass) {
        this.sfClass = sfClass;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carType='" + carType + '\'' +
                ", sfClass='" + sfClass + '\'' +
                '}';
    }
}
