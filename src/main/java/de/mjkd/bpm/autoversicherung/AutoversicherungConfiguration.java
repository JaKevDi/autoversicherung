package de.mjkd.bpm.autoversicherung;


import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.spring.boot.starter.util.SpringBootProcessEnginePlugin;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

/**
 * Configuration Klasse welche benutzt wird um den Camunda Spring Boot Prozess zu konfigurieren.
 * Konkret wird hier lediglich die möglichkeit der Benutzerkontensteuerung und Authorization aktiviert.
 * Ohne die Konfiguration wäre jeder User standardmäßig Administrator.
 */
@Configuration
public class AutoversicherungConfiguration extends SpringBootProcessEnginePlugin {

    public static final Logger LOGGER = Logger.getLogger(AutoversicherungConfiguration.class.getName());

    /**
     * Methode zur Aktivierung der Authorization bevor der Camunda Prozess erstellt wird.
     * @param processEngineConfiguration
     */
    @Override
    public void preInit(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super.preInit(processEngineConfiguration);
        LOGGER.info("++++++++++++++++++++++++++++++ Pre Init Call Config+++++++++++");
        if(!processEngineConfiguration.isAuthorizationEnabled()) {
            LOGGER.info("Not enabled");
            processEngineConfiguration.setAuthorizationEnabled(true);
        } else {
            LOGGER.info("Enabled");
        }
    }
}
