package de.mjkd.bpm.delegator;

import de.mjkd.bpm.ProcessConstants;
import de.mjkd.bpm.Service.SendEmailService;
import de.mjkd.bpm.model.Vertrag;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import javax.inject.Named;


public class EmailAdapter implements JavaDelegate {

    @Named("emailAdapter")
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        SendEmailService sendEmailService = new SendEmailService();

        String mailtext = (String)delegateExecution.getVariable("mailText");
        String subject = (String)delegateExecution.getVariable("mailBetreff");
        Vertrag vertrag = (Vertrag)delegateExecution.getVariable(ProcessConstants.VARIABLE_NAME_VERTRAG);
        String email = vertrag.getVersicherungsnehmer().getEmail();

        sendEmailService.sendEmail(email, mailtext, subject);
    }
}
