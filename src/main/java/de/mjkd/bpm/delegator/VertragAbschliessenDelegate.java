package de.mjkd.bpm.delegator;

import de.mjkd.bpm.ProcessConstants;
import de.mjkd.bpm.model.Vertrag;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Einfache Delegate Klasse welche für den ServiceTask VertragAbschliessen benötigt wird.
 * Gibt einfache Logger ausgabe aus.
 * Hier könnten verschiedene Dinge abgespeichert werden z.B. in einer Datenbank.
 */
public class VertragAbschliessenDelegate implements JavaDelegate {

    public static final Logger LOGGER = Logger.getLogger(VertragAbschliessenDelegate.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        Vertrag vertrag = (Vertrag)delegateExecution.getVariable(ProcessConstants.VARIABLE_NAME_VERTRAG);
        LOGGER.info("Vertrag " + vertrag.getVertragsNummer() + " akzeptiert und eingepflegt.");
    }
}
