package de.mjkd.bpm.delegator;

import org.camunda.bpm.BpmPlatform;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.Execution;

import java.util.logging.Logger;

/**
 * Bei angabe der Falschen ReferenzID wird eine Fehlermeldung beim Start des Tasks geladen.
 */
public class DokumentSendenErrorDelegate implements JavaDelegate {

    public static final Logger LOGGER = Logger.getLogger(DokumentSendenErrorDelegate.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        Execution tmpExecution = BpmPlatform.getDefaultProcessEngine().getRuntimeService().createExecutionQuery()
                .messageEventSubscriptionName("MSG_RECEIVED")
                .singleResult();
        if (tmpExecution == null) {
            LOGGER.info("Is NULL!");
            return;
        }
        String errorCode = (String)BpmPlatform.getDefaultProcessEngine().getRuntimeService().getVariable(tmpExecution.getProcessInstanceId(),"wrongRefID");
        if(errorCode == null) {
            LOGGER.info("ErrorCode is NULL");
        } else {
            delegateExecution.setVariable("wrongRefID", errorCode);
            LOGGER.info("ErrorCode:" + errorCode);
        }
    }
}
