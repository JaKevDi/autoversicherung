package de.mjkd.bpm.delegator;

import de.mjkd.bpm.ProcessConstants;
import de.mjkd.bpm.model.Car;
import de.mjkd.bpm.model.Person;
import de.mjkd.bpm.model.Vertrag;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.Variables;

import java.util.logging.Logger;

/**
 * Delegate Klasse welche am Ende des Prozessstartereignisses ausgeführt wird.
 * Überführt die Variablen der Eingabeform in ein Klassenobjekt und gibt dieses als Variable im Prozess bekannt.
 */
public class RequestDelegate implements JavaDelegate {

    public static final Logger LOGGER = Logger.getLogger(RequestDelegate.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        LOGGER.info("Past Form Delegator");
        String name = (String)delegateExecution.getVariable("customerName");
        String email = (String)delegateExecution.getVariable("customerEmail");
        int age = (int)delegateExecution.getVariable("customerAge");
        String gender = (String)delegateExecution.getVariable("customerGender");

        String carType = (String)delegateExecution.getVariable("carType");
        int sfClass = (int)delegateExecution.getVariable("sfClass");

        String price = (String)delegateExecution.getVariable("price");

        Car car = new Car();
        car.setCarType(carType);
        car.setSfClass(sfClass);

        Person person = new Person();
        person.setName(name);
        person.setEmail(email);
        person.setAge(age);
        person.setGender(gender);

        Vertrag vertrag = new Vertrag();

        vertrag.setProdukt("Deluxe Auto Versicherung");
        vertrag.setAuto(car);
        vertrag.setBeitragString(price);
        vertrag.setVersicherungsnehmer(person);
        double beitrag = Double.parseDouble(price.substring(0, price.length()-2));

        vertrag.setBeitrag(beitrag);

        LOGGER.info("Vertrag: " + vertrag);

        delegateExecution.setVariable(ProcessConstants.VARIABLE_NAME_VERTRAG, Variables.objectValue(vertrag).serializationDataFormat(Variables.SerializationDataFormats.JSON).create());

    }
}
