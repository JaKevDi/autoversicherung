package de.mjkd.bpm.delegator;

import de.mjkd.bpm.ProcessConstants;
import org.camunda.bpm.BpmPlatform;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.FileValue;

import java.io.ByteArrayInputStream;
import java.util.logging.Logger;

/**
 * Delegate der Ausgeführt wird, wenn der Task Dokument Senden beendet wurde.
 * Gibt das Hochgeladene Dokument vom aktuellen Prozess hoch an den Aufrufenden Prozess.
 * Benachrichtigt den Aufrufenden Prozess, dass das Dokument gesendet wurde und der Prozess fortgeführt werden kann.
 */
public class DokumentSendenDelegate implements JavaDelegate {

    public static final Logger LOGGER = Logger.getLogger(DokumentSendenDelegate.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        LOGGER.info("Dokument gesendet!");
        String refId = (String)delegateExecution.getVariable("refID");
        LOGGER.info(refId);

        LOGGER.info(delegateExecution.getVariable("dokumentRequested").getClass().getName());

        //Einlesen der Datei
        ByteArrayInputStream byteArrayInputStream = (ByteArrayInputStream)delegateExecution.getVariable("dokumentRequested");

        //Umwandlung in ein File Objekt
        FileValue document = Variables
                .fileValue("document.pdf")
                .file(byteArrayInputStream)
                .mimeType("application/pdf")
                .create();

        //Laden der Übergeordneten Instanz um zu Prüfen ob die ReferenzIDs identisch sind.
        Execution execution = BpmPlatform.getDefaultProcessEngine().getRuntimeService().createExecutionQuery()
                .messageEventSubscriptionName("MSG_RECEIVED")
                .processVariableValueEquals(ProcessConstants.VARIABLE_NAME_REFID, refId)
                .singleResult();


        if (execution == null) {
            LOGGER.info("Exection is NULL, Ref ID not match. Restart Process");
            Execution restartExecution = BpmPlatform.getDefaultProcessEngine().getRuntimeService().createExecutionQuery()
                    .messageEventSubscriptionName("MSG_RECEIVED")
                    .singleResult();
            BpmPlatform.getDefaultProcessEngine().getRuntimeService().setVariable(
                    restartExecution.getProcessInstanceId(), "wrongRefID","Falsche ReferenzID angegeben"
            );
            BpmPlatform.getDefaultProcessEngine().getRuntimeService().createMessageCorrelation("MSG_REQUEST_RECEIVED")
                    .correlate();
            return;
        }
        LOGGER.info("Execution ID" + execution.getProcessInstanceId());

        // Dokument nach obenhin als Variable bekannt machen
        BpmPlatform.getDefaultProcessEngine().getRuntimeService().setVariable(
                execution.getProcessInstanceId(), "document", document
        );

        // Benachrichtigung des aufrufenden Prozess dass ein Dokument gesendet wurde.
        BpmPlatform.getDefaultProcessEngine().getRuntimeService().createMessageCorrelation("MSG_RECEIVED")
                .processInstanceVariableEquals(ProcessConstants.VARIABLE_NAME_REFID, refId)
                .correlateWithResult();

    }
}
