package de.mjkd.bpm.delegator;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Einfache Delegate Klasse welche für den ServiceTask VertragAblehnen benötigt wird.
 * Gibt einfache Logger ausgabe aus.
 * Hier könnten verschiedene Dinge abgespeichert werden z.B. in einer Datenbank.
 */
public class VertragAblehnenDelegate implements JavaDelegate {

    public static final Logger LOGGER = Logger.getLogger(VertragAblehnenDelegate.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        LOGGER.info("Riskio ist zu hoch. Der Vertrag wurde abgelehnt!");
    }
}
