package de.mjkd.bpm.Service;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

/**
 * Service zum Versenden einer Email über den Prozess.
 * Verwendet Apache Commons Library zum Versand der Email
 *
 */
public class SendEmailService {

    /**
     * Methode zum Versenden von Emails.
     * Hinterlegte Daten sind nur temporär
     *
     * @param toEmail Empfänger
     * @param mailtext Text der Mail
     * @param subject Betreff der Mail
     * @throws EmailException Exception im Fehlerfall
     */
    public void sendEmail(String toEmail, String mailtext, String subject) throws EmailException {

        Email email = new SimpleEmail();
        email.setHostName("bellatrix.uberspace.de");
        email.setSmtpPort(587);
        email.setAuthentication("raeste-bpmn", "bpmn-betries");
        email.setFrom("auto@ihreversicherung.de");
        email.setCharset("utf-8");
        email.setSubject(subject);
        email.setMsg(mailtext);
        email.addTo(toEmail);
        email.send();
    }
}
